import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: '', loadChildren: './login/login.module#LoginModule'},
            {path: 'register', loadChildren: './registration/registration.module#RegistrationModule'},
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
