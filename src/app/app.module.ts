import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MyMaterialModule} from './material.module';

// import { RouterModule } from '@angular/router';
import {CustomphoneComponent} from './customphone/customphone.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {TokenInterceptorService} from './services/token-interceptor.service';
import {RegisterService} from "./services/register.service";
import {MainModule} from './main/main.module';
import {MatTableModule} from '@angular/material/table';
import {AppRoutingModule} from "./app-routing.module";
import {PolicyinfoModule} from "./policyinfo/policyinfo.module";
import {InternationalPhoneNumberModule} from 'ngx-international-phone-number';
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {Ng2TelInputModule} from 'ng2-tel-input';
import {MatSelectModule} from "@angular/material/select";
import {FlexLayoutModule} from '@angular/flex-layout';
import {RegistrationModule} from "./registration/registration.module";
import {LoginModule} from "./login/login.module";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatListModule} from "@angular/material/list";
import {MatSidenavModule} from "@angular/material/sidenav";


@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        BrowserAnimationsModule,
        MyMaterialModule,
        AppRoutingModule,
        ReactiveFormsModule,
        MainModule,
        MatSnackBarModule,
        MatTableModule,
        MatGridListModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        PolicyinfoModule,
        RegistrationModule,
        LoginModule,
        InternationalPhoneNumberModule,
        MatAutocompleteModule,
        Ng2TelInputModule,
        MatSelectModule,
        FlexLayoutModule

    ],

    declarations: [
        AppComponent,
        CustomphoneComponent,
    ],
    providers: [RegisterService, {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptorService,
        multi: true
    }],

    exports: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
