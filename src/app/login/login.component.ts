import { Component, OnInit } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material/core';
import {Router} from "@angular/router";
import {MatSnackBar} from '@angular/material/snack-bar';
import {LoginService} from "../services/login.service";
import {Account} from "../model/account";
import {FormBuilder, FormControl, FormGroupDirective, NgForm, Validators} from "@angular/forms";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


    hide = true;
  constructor(private loginService: LoginService, private fb: FormBuilder,
              private router: Router, private snackBar: MatSnackBar) {


  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,

  ]);

  matcher = new MyErrorStateMatcher();
    message:any;
    public username:string;
    public password:string;


  ngOnInit() {
  }

  login() {
      const test='sravani.rampay@gmail.com';
      let account: Account = {
          'password': this.password,
          'username': this.username
      }
      localStorage.removeItem('token');
    this.loginService.loginUser(account)
        .subscribe(data => {
            localStorage.setItem('token', data.token);
            this.loginService.changeMessage(test);
          if (!data.error) {
            this.router.navigate(['main/policyinfo']).then(r => console.log());
          } else {
            this.loginService.openSnackBar('Error '+data.error, 'Invalid Username or password');
          }
          // this.router.navigateByUrl('/phoneConfim/'+data.id);
        }, error => {
          console.log('error message created' + error);
        });
  }

}
