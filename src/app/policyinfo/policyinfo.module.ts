import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MyMaterialModule} from "../material.module";
import {PolicyInfoRoutingModule} from "./policy-info-routing.module";
import {PolicyInfoComponent} from "./policy-info.component";
import {PolicyDetailsComponent} from "./policy-details/policy-details.component";
import {FlexModule} from "@angular/flex-layout";
import {MatTableModule} from "@angular/material/table";
import {PdfJsViewerModule} from "ng2-pdfjs-viewer";



@NgModule({
  declarations: [PolicyInfoComponent,PolicyDetailsComponent],
    imports: [
        CommonModule,
        MyMaterialModule,
        PolicyInfoRoutingModule,
        FlexModule,
        MatTableModule,
        PdfJsViewerModule,

    ]
})
export class PolicyinfoModule { }
