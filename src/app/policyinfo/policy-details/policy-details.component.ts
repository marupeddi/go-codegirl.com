import {Component, OnInit, ViewChild} from '@angular/core';
import {PolicyDetailsService} from "../../services/policy-details.service";
import {Router} from "@angular/router";
import {DocumentsService} from "../../services/documents.service";

@Component({
  selector: 'app-policy-details',
  templateUrl: './policy-details.component.html',
  styleUrls: ['./policy-details.component.css']
})
export class PolicyDetailsComponent implements OnInit {

  constructor(private policyDetailsService: PolicyDetailsService, private router: Router,
              private documentsService: DocumentsService) { }

  policies: any;
  data: any;
  documents: any;
  pdf: any;
  displayedColumns: string[] = ['name','actions'];
  @ViewChild('pdfViewer') public pdfViewer;

  ngOnInit() {
    this.policyDetailsService.currentMessage.subscribe(message => {
      this.data = message
    });

    this.documentsService.getDocument(this.data.id).subscribe(data =>{
      this.documents= data;
    });
  }



  back(){
    this.router.navigate(['main/policyinfo']);
  }


  showDocument(scanned: any) {
    const httpOptions = {
      'responseType'  : 'arraybuffer' as 'json'
    };
    this.documentsService.getPdf(scanned, httpOptions).subscribe(data =>{
      let file = new Blob([data], { type: 'application/pdf' });
      let fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    });

  }
}
