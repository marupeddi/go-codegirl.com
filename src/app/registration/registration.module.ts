import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailConfirmationComponent} from './email-confirmation/email-confirmation.component';
import { PhoneConfirmationComponent} from './phone-confirmation/phone-confirmation.component';
import { ContactInfoComponent } from './contact-info/contact-info.component';
import {RegistrationRoutingModule} from "./registration-routing.module";
import {RegistrationComponent} from "./registration.component";
import {MyMaterialModule} from "../material.module";
import {ReactiveFormsModule} from "@angular/forms";
import {MatSelectModule} from "@angular/material/select";


@NgModule({
  declarations: [EmailConfirmationComponent,PhoneConfirmationComponent,ContactInfoComponent,RegistrationComponent],
  imports: [
    CommonModule,
    RegistrationRoutingModule,
    MyMaterialModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class RegistrationModule { }
