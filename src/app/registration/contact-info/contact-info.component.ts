import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {LoginService} from "../../services/login.service";
import {PolicyDetailsService} from "../../services/policy-details.service";

@Component({
  selector: 'app-practice-policy',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.css']
})
export class ContactInfoComponent implements OnInit {
    policies: any;
  policy: any;

  constructor(private loginService: LoginService,
              private policyDetailsService:PolicyDetailsService,
              private router: Router) { }

  onLogout(){
    this.router.navigate(['./']);
  }

  ngOnInit() {
    this.loginService.currentMessage.subscribe(message => this.policies = message);
  }

  protected getMoreInfo(titleData:any){
    this.policyDetailsService.changeMessage(titleData);
    this.router.navigate(['./main/policyinfo/policyDetails']);
  }

}
