
import {Injectable} from "@angular/core";
import {BaseService} from "./base-service";
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {User} from "../model/user";
import {VerifyCode} from "../model/verifyCode";
import {HttpHeaders} from "@angular/common/http";
import {MatSnackBar} from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RegisterService extends BaseService{


  constructor(public http: HttpClient,
              public route: ActivatedRoute,
              public router: Router,public snackBar: MatSnackBar) {
    super(http,route , router, snackBar);
  }


  registerUser(user: User): Observable<any> {
    return super.postData(this.teloswsUrl + 'api/auth/register', user);
  }

  confirmCode(verifyCode: VerifyCode): Observable<boolean> {
    return super.postData(this.teloswsUrl + 'api/auth/verifyPhoneCode', verifyCode);
  }

}