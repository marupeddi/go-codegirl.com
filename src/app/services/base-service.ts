import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {throwError} from "rxjs";
import {catchError, retry} from "rxjs/operators";
import { environment } from '../../environments/environment';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as _ from 'lodash';


export class BaseService {

    constructor(public http: HttpClient,
                public route: ActivatedRoute,
                public router: Router,public snackBar: MatSnackBar) { }

    public teloswsUrl:string =environment.apiUrl;

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 3000,
        });
    }

     protected httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    protected static handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        window.alert(errorMessage);
        return throwError(errorMessage);
    }

    protected getData(url: string, httpOption?: any) {
        let options = !_.isNil(httpOption) ? httpOption : this.httpOptions;
        return this.http.get<any>(url, options)
            .pipe(
                retry(1),
                catchError(BaseService.handleError)
            );
    }

    protected postData(url: string,data: any) {
        return this.http.post<any>(url, data, this.httpOptions)
            .pipe(
                retry(1),
                catchError(BaseService.handleError)
            );
    }


}
