import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {MainComponent} from "./main.component";
import {PolicyInfoComponent} from "../policyinfo/policy-info.component";
import {ContactInfoComponent} from "../registration/contact-info/contact-info.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'main', component: MainComponent,
                children: [
                    {
                        path: 'policyinfo',
                        loadChildren: '../policyinfo/policyinfo.module#PolicyinfoModule'
                    },
                    {
                        path: 'contact',
                        component: ContactInfoComponent
                    }

                ],
            },
            {
                 path: 'policyinfo', component:PolicyInfoComponent
            },



        ])
    ],
    exports: [RouterModule]
})
export class MainRoutingModule {

}
